%global glib2_version 2.58
%global colord_version 1.4.5
%global geocode_glib_version 3.26.3
%global gnome_desktop_version 3.37.1
%global gsettings_desktop_schemas_version 42
%global gtk3_version 3.15.3
%global geoclue_version 2.3.1

%global systemd_units org.gnome.SettingsDaemon.A11ySettings.service org.gnome.SettingsDaemon.Color.service org.gnome.SettingsDaemon.Datetime.service org.gnome.SettingsDaemon.Housekeeping.service org.gnome.SettingsDaemon.Keyboard.service org.gnome.SettingsDaemon.MediaKeys.service org.gnome.SettingsDaemon.Power.service org.gnome.SettingsDaemon.PrintNotifications.service org.gnome.SettingsDaemon.Rfkill.service org.gnome.SettingsDaemon.ScreensaverProxy.service org.gnome.SettingsDaemon.Sharing.service org.gnome.SettingsDaemon.Smartcard.service org.gnome.SettingsDaemon.Sound.service org.gnome.SettingsDaemon.UsbProtection.service org.gnome.SettingsDaemon.Wwan.service org.gnome.SettingsDaemon.XSettings.service

%global tarball_version %%(echo %{version} | tr '~' '.')
%global major_version %%(echo %{version} | cut -f 1 -d '~' | cut -f 1 -d '.')

Name:           gnome-settings-daemon
Version:        44.1
Release:        2
Summary:        The daemon sharing settings from GNOME to GTK+/KDE applications
License:        GPLv2+
URL:            https://gitlab.gnome.org/GNOME/gnome-settings-daemon
Source0:        https://download.gnome.org/sources/%{name}/%{major_version}/%{name}-%{tarball_version}.tar.xz
Source1:        org.gnome.settings-daemon.plugins.power.gschema.override


BuildRequires:  cups-devel gcc gettext meson >= 0.49.0 perl-interpreter pkgconfig(alsa)
BuildRequires:  pkgconfig(colord) >= %{colord_version} pkgconfig(fontconfig) pkgconfig(geoclue-2.0) >= %{geoclue_version}
BuildRequires:  pkgconfig(geocode-glib-2.0) >= %{geocode_glib_version} pkgconfig(glib-2.0) >= %{glib2_version}
BuildRequires:  pkgconfig(gnome-desktop-3.0) >= %{gnome_desktop_version} pkgconfig(gsettings-desktop-schemas) >= %{gsettings_desktop_schemas_version}
BuildRequires:  pkgconfig(gtk+-3.0) >= %{gtk3_version} pkgconfig(gudev-1.0) pkgconfig(gweather4)
BuildRequires:  pkgconfig(lcms2) >= 2.2 pkgconfig(libcanberra-gtk3) pkgconfig(libgeoclue-2.0)
BuildRequires:  pkgconfig(libnm) pkgconfig(libnotify) pkgconfig(libpulse) pkgconfig(libpulse-mainloop-glib)
BuildRequires:  pkgconfig(librsvg-2.0) pkgconfig(nss) pkgconfig(polkit-gobject-1) pkgconfig(upower-glib)
BuildRequires:  pkgconfig(x11) pkgconfig(xi) pkgconfig(wayland-client)
BuildRequires:  pkgconfig(libwacom) >= 0.7 pkgconfig(xorg-wacom) pkgconfig(mm-glib) chrpath
BuildRequires:  pkgconfig(gcr-4)

Requires:       colord >= %{colord_version} geoclue2 >= %{geoclue_version} geocode-glib2 >= %{geocode_glib_version} glib2 >= %{glib2_version}
Requires:       gnome-desktop3 >= %{gnome_desktop_version} gsettings-desktop-schemas >= %{gsettings_desktop_schemas_version}
Requires:       gtk3 >= %{gtk3_version} iio-sensor-proxy pkgconfig(gweather4)

%description
The GNOME Settings Daemon is responsible for setting various parameters of a
GNOME Session and the applications that run under it.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}

%description    devel
gnome-settings-daemon-devel provides static libraries,head files,test cases
and other related development files of gnome-settings-daemon.

%prep
%autosetup -p1

%build
%meson
%meson_build

%install
%meson_install

install -p %{SOURCE1} $RPM_BUILD_ROOT%{_datadir}/glib-2.0/schemas

%find_lang %{name} --with-gnome

install -d $RPM_BUILD_ROOT%{_libdir}/gnome-settings-daemon-3.0/gtk-modules

chrpath -d %{buildroot}%{_libexecdir}/*
mkdir -p %{buildroot}%{_sysconfdir}/ld.so.conf.d
echo "%{_libdir}/gnome-settings-daemon-%{major_version}" > %{buildroot}%{_sysconfdir}/ld.so.conf.d/%{name}-%{_arch}.conf

%post
%systemd_user_post %{systemd_units}

%preun
%systemd_user_preun %{systemd_units}

%files -f %{name}.lang
%license COPYING
%doc AUTHORS NEWS README

%{_libexecdir}/gsd-*
%{_sysconfdir}/xdg/autostart/*
%{_sysconfdir}/xdg/Xwayland-session.d/00-xrdb
%{_userunitdir}/
%{_datadir}/glib-2.0/schemas/*
%{_datadir}/polkit-1/actions/*
%{_libdir}/gnome-settings-daemon-%{major_version}/libgsd.so
%{_sysconfdir}/ld.so.conf.d/%{name}-%{_arch}.conf
%{_udevrulesdir}/61-gnome-settings-daemon-rfkill.rules

/usr/lib/udev/rules.d/*.rules
%{_datadir}/gnome-settings-daemon/
%{_datadir}/GConf/gsettings/gnome-settings-daemon.convert

%files devel
%{_includedir}/gnome-settings-daemon-%{major_version}
%{_libdir}/pkgconfig/gnome-settings-daemon.pc

%changelog
* Tue Sep 10 2024 beta <beta@yfqm.date> - 44.1-2
- Fix ldconfig search path

* Mon Nov 27 2023 lwg <liweiganga@uniontech.com> - 44.1-1
- update to version 44.1

* Fri Feb 10 2023 Wenlong Ding <wenlong.ding@turbolinux.com.cn> - 43.0-2
- Add missing global define %{systemd_units}

* Mon Jan 02 2023 lin zhang <lin.zhang@turbolinux.com.cn> - 43.0-1
- Update to 43.0

* Mon Jun 6 2022 lin zhang <lin.zhang@turbolinux.com.cn> - 42.2-2
- modify the libgsd.so search path to /usr/lib64/gnome-settings-daemon-42

* Mon Jun 6 2022 lin zhang <lin.zhang@turbolinux.com.cn> - 42.2-1
- Upgrade to 42.2

* Thu Sep 09 2021 lingsheng <lingsheng@huawei.com> - 3.38.2-2
- Delete rpath setting

* Mon May 24 2021 weijin deng <weijin.deng@turbolinux.com.cn> - 3.38.2-1
- Upgrade to 3.38.2
- Update Version, Release, stage 'files'

* Fri Nov 29 2019 zhouyihang <zhouyihang1@huawei.com> - 3.30.1.2-2
- Package init
